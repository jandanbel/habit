<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.3.0
 */

if (!defined('ABSPATH')) {
    exit;
}

get_template_part('woocommerce/theme-custom/order', 'step');
wc_print_notices();
?>
<div style="text-align:center">
  <h3>
    Complete your order now by clicking 'Pay with CheckMeOut'. <br/>No need to sign up!
  </h3>
</div>
<div class="wrap-checkout-notices"><?php

    do_action('woocommerce_before_checkout_form', $checkout);

    // If checkout registration is disabled and not logged in, the user cannot checkout
    if (!$checkout->enable_signup && !$checkout->enable_guest_checkout && !is_user_logged_in()) {
        echo apply_filters('woocommerce_checkout_must_be_logged_in_message', __('You must be logged in to checkout.', 'woocommerce'));
        return;
    } ?>
</div>


<?php do_action('woocommerce_after_checkout_form', $checkout); ?>
