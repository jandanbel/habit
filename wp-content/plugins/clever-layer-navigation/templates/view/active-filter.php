<?php
/**
 */

$display = 'none';
if (isset($selected_filter_option) && count($selected_filter_option)){
    if( count($selected_filter_option)==1&& isset($selected_filter_option['orderby'])){
        $display = 'none';
    }else
    $display = 'block';
}

$filter_id = mt_rand();
?>
<div id="cln-filter-item-<?php echo esc_attr($filter_id) ?>" class="zoo-filter-block zoo-active-filter"  style="display: <?php echo $display;?>">
    <h4 class="zoo-title-filter-block"><?php echo esc_html($content_data['title']); ?></h4>
    <ul class="zoo-ln-wrap-activated-filter">
        <?php
        if (isset($selected_filter_option) && count($selected_filter_option)) {
            require ZOO_LN_TEMPLATES_PATH . 'view/active-filter/items.php';
        }
        ?>
    </ul>
</div>
